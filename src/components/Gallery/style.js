import styled from 'styled-components';
import { tablet, monitor} from '../../styles/mediaQueries';


export const GalleryWrapper = styled.div`
  padding-top: 5vh;
  width: 100%;
  display: flex;
  flex-flow: row wrap;
`;

export const GalleryItem = styled.div`
  width: 100%;
  margin: 1rem;
  box-shadow: 1px 2px 5px 2px rgba(160, 160, 160, .5);
  ${tablet} {
    flex-basis: calc(50% - 2rem);
  }
  ${monitor} {
    flex-basis: calc((100% /3) - 2rem);
  }
`;

export const GalleryImage = styled.img`
  height: 100%;
  width: 100%;
  display: block;
`;
