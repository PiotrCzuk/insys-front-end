import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import Loader from '../Loader/Loader'
import {fetchPhotos} from "../../store/actions/index";

import {GalleryImage, GalleryItem, GalleryWrapper} from "./style";

class Gallery extends React.Component {
    static propTypes = {
        fetchPhotos: PropTypes.func.isRequired,
        srcPath: PropTypes.string,
        key: PropTypes.string,
        farm: PropTypes.string,
        server: PropTypes.string,
        id: PropTypes.string,
        secret: PropTypes.string,
    };

    componentDidMount() {
        this.props.fetchPhotos();
    }

    renderGallery = () => {
        return this.props.photos.photo.map(({farm, server, id, secret}) => {
            let srcPath = `https://farm${farm}.staticflickr.com/${server}/${id}_${secret}.jpg`;
            return (
                <GalleryItem key={id}>
                    <a href={srcPath} target="_blank" rel="noopener noreferrer">
                        <GalleryImage src={srcPath} alt="Marlin"/>
                    </a>

                </GalleryItem>
            )
        })
    };


    render() {
        return (
            <>
                {this.props.photos
                    ? (
                        <GalleryWrapper>
                            {this.renderGallery()}
                        </GalleryWrapper>)
                    : <Loader/>
                }
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        photos: state.gallery.photos
    }
};

export default connect(mapStateToProps, {fetchPhotos})(Gallery);