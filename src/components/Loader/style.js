import styled, {keyframes} from 'styled-components';


const flash = keyframes`
  0% {
    opacity: .2;
  }
  20% {
    opacity: 1;
  }
  100% {
    opacity: .2;
  }
`;

export const Animated = styled.div`
  text-align: left;
  span {
    color: black;
    display: inline-block;
    margin-top: 50px;
    margin-left: 4px;
    margin-right: 4px;
    font-size: 80px;
    line-height: 0.1;
    animation-name: ${flash};
    animation-duration: 1s;
    animation-iteration-count: infinite;
    animation-fill-mode: both;
  }
  span:nth-child(2) {
    animation-delay: 0.2s;
  }
  span:nth-child(3) {
    animation-delay: 0.4s;
  }
`;