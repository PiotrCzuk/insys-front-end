import React from 'react';

import {Animated} from "./style";

const Loader = () => (
    <Animated>
        <span>.</span>
        <span>.</span>
        <span>.</span>
    </Animated>
);


export default Loader;