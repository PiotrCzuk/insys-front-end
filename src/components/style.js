import styled from 'styled-components';
import { tablet} from '../styles/mediaQueries';

export const Wrapper = styled.div`
  width: 85%;
  margin-left: auto;
  margin-right: auto;
  height: 100%;
  overflow: hidden;
  padding-bottom: 100px;
  margin-bottom: 150px;
  border-bottom: 3px solid gainsboro;
  
  ${tablet} {
  width: 90%;
  }
`;
