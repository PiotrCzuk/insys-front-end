import React from 'react';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faMapMarkerAlt} from '@fortawesome/free-solid-svg-icons';

import {ArticleSection, Header, Position, Quotation} from "./style";

const Content = () => {

    return (
        <>
        <Header>
            Marlin Monroe
        </Header>
        <Position>
            <FontAwesomeIcon icon={faMapMarkerAlt}/> Poznań, PL
        </Position>
        <ArticleSection>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi aut autem culpa deleniti, deserunt
            doloremque
            esse et explicabo facere facilis id officiis omnis praesentium quis reiciendis sed vero. Delectus.
        </ArticleSection>
        <Quotation>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum enim laborum necessitatibus omnis sit.
            Aspernatur at autem distinctio eum,
            facere impedit necessitatibus obcaecati quas reiciendis sequi veritatis voluptatem? Fuga, suscipit?
        </Quotation>
        <ArticleSection>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi aut autem culpa deleniti, deserunt
            doloremque
            esse et explicabo facere facilis id officiis omnis praesentium quis reiciendis sed vero. Delectus.
        </ArticleSection>
        </>
    )
};

export default Content;