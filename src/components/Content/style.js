import styled from 'styled-components';

import {colorsMain} from "../../styles/colors";


export const Header = styled.h1`
  font-weight: 700;
  font-size: 2em;
  color: black;
  padding-top: auto;
`;

export const Position = styled.div`
  color: grey;
  font-size: 1.2em;
`;

export const Quotation = styled.div`
  color: grey;
  font-style: italic;
  margin-left: 1em;
  padding-left: 15px;
  border-left: 6px solid ${colorsMain.backgroundSecondary}
  width: 70%
`;

export const ArticleSection = styled.div`
  color: black;
  padding: 20px 0;
`;