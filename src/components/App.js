import React from 'react';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';

import Header from './Header/Header';
import Content from './Content/Content';
import Gallery from './Gallery/Gallery';

import {Wrapper} from './style';

const App = () => {
    return (
        <div>
            <BrowserRouter>
                    <Header/>
                    <Wrapper>
                        <Switch>
                            <Route path="/" exact component={Content}/>
                            <Route path="/gallery" exact component={Gallery}/>
                            <Redirect to="/"/>
                        </Switch>
                    </Wrapper>
            </BrowserRouter>
        </div>
    )
};


export default App;