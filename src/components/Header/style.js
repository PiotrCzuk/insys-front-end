import styled from 'styled-components';
import Stones from '../../assets/img/Stones.jpg'
import {NavLink} from 'react-router-dom';

import {colorsMain} from "../../styles/colors";


export const HeaderImage = styled.div`
    background: url(${Stones}) no-repeat 50% 50%;
    width: 100%;
    height: 15vh;
    vertical-align: middle;
    background-size: 100% auto;
`;

export const NavigationBackground = styled.nav`
  background-color: ${colorsMain.backgroundSecondary};
`;

export const NavigationWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  width: 85%;
  margin-left: auto;
  margin-right: auto;
  align-items: center;
  position: relative;
`;

export const CircularPortrait = styled.div`
  position: absolute;
    bottom: -15px;
    width: 130px;
`;

export const PortraitImage = styled.img`
  width: 100%;
  height: auto;
  border-radius: 50%;
  border: 7px solid rgba(255,255,255,0.4);
`;

export const Navigation = styled.ul`
  margin-left: auto;
  padding: 0;
  list-style: none;
  color: white;
  font-weight: 700;
  display: flex;
`;

export const NavigationItem = styled.li`
  margin-right: 25px;
  padding: 0;
  font-size: 1.2em;
`;

export const StyledLink = styled(NavLink)`
  &.active {
    border-radius: 50%;
    border: 2px solid white;
    padding: 5px;
    }
`;