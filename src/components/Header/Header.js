import React from 'react';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faImage, faUser} from '@fortawesome/free-solid-svg-icons';
import Marlin from '../../assets/img/Marlin.jpg';

import {StyledLink, HeaderImage, PortraitImage, CircularPortrait, Navigation, NavigationBackground, NavigationItem, NavigationWrapper} from "./style";

const Header = () => {
    return (
        <>
            <HeaderImage/>
            <NavigationBackground>
                <NavigationWrapper>
                    <CircularPortrait>
                        <PortraitImage src={Marlin} alt="Marlin"/>
                    </CircularPortrait>
                    <Navigation>
                        <NavigationItem>
                            <StyledLink exact to="/">
                                <FontAwesomeIcon icon={faUser}/>
                            </StyledLink>
                        </NavigationItem>
                        <NavigationItem>
                            <StyledLink exact to="/gallery">
                                <FontAwesomeIcon icon={faImage}/>
                            </StyledLink>
                        </NavigationItem>
                    </Navigation>
                </NavigationWrapper>
            </NavigationBackground>
        </>

    )
};

export default Header;