import flickr from '../../services/flickr';
import {FETCH_PHOTOS} from "./types";

export const fetchPhotos = () => async dispatch => {
    const API_KEY = process.env.REACT_APP_INSYS_API_KEY;
    const response = await flickr.get(`?method=flickr.photos.search&api_key=${API_KEY}&tags=MarylinMonroe&per_page=9&page=1&format=json&nojsoncallback=1`);

    dispatch({type: FETCH_PHOTOS, payload: response.data})
};

