export const colorsMain = {
  background: '#FFFEFE',
  backgroundSecondary: '#74D6BD',
  text: '#000'
};